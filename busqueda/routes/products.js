const express = require('express');
const router = express.Router();
const product = require('../controllers/ProductController.js');

router.get('/save', product.save);

module.exports = router;