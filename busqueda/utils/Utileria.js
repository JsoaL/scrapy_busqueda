const exec = require('child_process').exec;
const replace = require('replace-in-file');
const fetch = require("node-fetch");
const Utileria = {};

Utileria.execute =  (command, funcSucs, funcError)=> {
    exec(command, (error, stdout)=> {
        if (error !== null) {console.log('exec error: ' + error);funcError();}
        else { funcSucs(); }
    });
};

Utileria.changeFile = (from,to,file)=>{
    const options = {
        files: file,
        from: new RegExp(from, "g"),
        to: to,
    };
    try {
        return replace.sync(options); 
    } catch (error) { console.error('Error occurred:', error); }
};

Utileria.asyncGetReq = async (datos, url) =>{
    let params = '?' + Object.keys(datos).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(datos[k])).join('&');
    let response = await fetch(`${url}${params}`);
    if (!response.ok)
        throw Error(response.statusText);
    else
        return await response.json();
};

module.exports = Utileria;