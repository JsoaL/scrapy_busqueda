# -*- coding: utf-8 -*-
import scrapy


class DictionarySpider(scrapy.Spider):
    name = 'dictionary'
    allowed_domains = ['conceptodefinicion.de']
    start_urls = ['https://conceptodefinicion.de/@@@/']

    def start_requests(self):
        url = "https://conceptodefinicion.de/@@@/"
        # Set the headers here. The important part is "application/json"
        headers =  {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36',
            'Accept': 'application/json,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, sdch',
            'Accept-Language': 'en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4',
        }

        yield scrapy.http.Request(url, headers=headers)

    def parse(self, response):
        article = response.xpath('//div[@class="tb"]/p')
        for item in article:
            img = item.xpath('.//img/@src').extract_first()
            if img:
                break
        concepto = response.xpath("//article/header/h1/text()").extract_first()
        definicion = response.xpath('//div[@class="tb"]/p[1]').extract_first()
        yield{
                'concepto': concepto,
                'definicion': definicion,
                'imagen': img
            }
