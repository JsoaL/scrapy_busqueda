var express = require('express');
var router = express.Router();
const main = require('../controllers/MainController.js');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('clients/index', {carrusel: false});
});
router.post('/buscar', main.doSearch);
router.get('/test', main.test);
router.get('/servicioDiccionario', main.servicioDiccionario);
router.get('/servicioCompras', main.servicioCompras);

module.exports = router;
