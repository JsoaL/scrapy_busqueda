const Product = require("../models/Product");
const ProductController = {};

ProductController.create = function (req, res){res.render('../views/admin/create');};

ProductController.save = function (req, res){
    var producto = new Product(req.body);
    producto.save(function (err) {
        if (err) { console.log('Error: ', err); return; }
        console.log("Successfully created a user.");
        res.redirect("/admin/create");
    });
};

module.exports = ProductController;