const express = require('express');
const router = express.Router();
const concept = require('../controllers/ConceptController.js');

router.get('/save', concept.save);
router.get('/create', concept.create);

module.exports = router;