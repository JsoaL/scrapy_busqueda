# -*- coding: utf-8 -*-
import scrapy
from prueba.items import PruebaItem

class EjemploSpider(scrapy.Spider):
    name = 'ejemplo' #nombre de la araña
    allowed_domains = ['amazon.com.mx'] #dominio, puede haber varios dominios
    start_urls = ['https://www.amazon.com.mx/s?k=@@@']

    def start_requests(self):
        url = "https://www.amazon.com.mx/s?k=@@@"
        # Set the headers here. The important part is "application/json"
        headers =  {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.84 Safari/537.36',
            'Accept': 'application/json,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, sdch',
            'Accept-Language': 'en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4',
        }

        yield scrapy.http.Request(url, headers=headers)

    #url de la pagina
    def parse(self, response):
        articles = response.xpath('/html/body/div[1]/div[1]/div[1]/div[2]/div/span[4]/div[1]/div/div')
        for article in articles:
            producto = article.xpath('.//span[@class="a-size-medium a-color-base a-text-normal"]/text()').extract_first()
            if not producto:
                producto = article.xpath('.//span[@class="a-size-base-plus a-color-base a-text-normal"]/text()').extract_first()
            precio = article.xpath('.//span[@class="a-price"]/span[@class="a-offscreen"]/text()').extract_first()
            imagen = article.xpath('.//div[@class="a-section aok-relative s-image-fixed-height"]/img/@src').extract_first()
            if not imagen:
                imagen = article.xpath('.//div[@class="a-section aok-relative s-image-square-aspect"]/img/@src').extract_first()
            yield{
                'producto': producto,
                'precio': precio,
                'imagen': imagen
            }
        next_page = 'https://www.amazon.com.mx/' + response.xpath('//li[@class="a-last"]/a/@href').extract_first()
        print(next_page)
        yield scrapy.Request(next_page, callback = self.parse)