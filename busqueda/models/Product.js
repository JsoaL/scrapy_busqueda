const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    producto: String,
    precio: String,
    imagen: String
});

module.exports = mongoose.model('Product', ProductSchema);