//Funciones Globales
/*
asyncgetReq(datosJson,url)
        .then( res =>{
            console.log({res});
        }).catch(e => console.error(e));
 */
//Peticion Asincrona GET - JSON
const asyncGetReq = async (datos, url) =>{
    let params = '?' + Object.keys(datos).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(datos[k])).join('&');
    let response = await fetch(`${url}${params}`);
    if (!response.ok)
        throw Error(response.statusText);
    else
        return await response.json();
};

/*
asyncPostReq(datosJson,url,contentType)
        .then( res =>{
            console.log({res});
        }).catch(e => console.error(e));
 */
//Peticion Asincrona POST - JSON
const asyncPostReq = async (datos, url, cntntType) =>{
    let response = await fetch(url, {
        method: 'POST',
        body: JSON.stringify(datos),
        headers: { 'Content-Type': cntntType } //'application/json' 'multipart/form-data'
    });
    if (!response.ok)
        throw Error(response.statusText);
    else
        return await response.json();
};

//Limpiar limpiar imputs...
const trimUp = params => {
    for (const key in params) {
        if (!params[key] || (typeof params[key] === 'string' && (params[key]).trim().length === 0)){
            delete params[key];
        }
        else
            if (typeof params[key] === 'string')
                params[key] = params[key].trim().toUpperCase();
    }
};
//Notificacion
const createNotify = (icon,message,type,timer,delay) =>{
    $.notify({
        icon: icon,
        message: message
    }, {
        type: type,
        timer: timer,
        delay: delay,
        placement: {
            from: "top",
            align: "center"
        },
        onShow: null,
        onShown: null,
        onClose: null,
        onClosed: null
    });
};

const resetForm = () => {
    document.getElementById('idFormNewUser').reset();
};

$('#btnLogout').click((e)=>{ //centralModalSm // btnLogoutModal
    e.preventDefault();
    $('#centralModalSm').modal({ //Desplegar modal
        backdrop: 'static',
        keyboard: false,
        show: true
    });
});

$('#btnLogoutModal').click(()=>{
    asyncGetReq({logout:true}, '/logout').then( data =>{
        if (data) {
            if (data.status === "F") {
                createNotify('pe-7s-close-circle', `Error, <b>INTENTE de nuevo</b>.`, 'danger', 1000, 5000);
            } else {
                $('#centralModalSm').modal('hide');
                createNotify('pe-7s-check', `<center><b>Sesion Cerrada.</b></center>`, 'success', 1000, 2000);
                setTimeout(() => { $(location).attr('href','/'); }, 3000);
            }
        }
    }).catch(e => console.error(e));
});

$('#btnLogin').click(()=>{
    const params = {
        user: $('#textUsuario').val().trim(),
        pass: $('#textContra').val().trim()
    };
    let url = "admin/login";
    asyncPostReq(params, url, 'application/json').then( data =>{
        if (data) {
            if (data.status === "F") {
                createNotify('pe-7s-close-circle', `Datos incorrectos, <b>INTENTE de nuevo</b>.`, 'danger', 1000, 5000);
                $('#formLogin').trigger("reset");
            } else {
                createNotify('pe-7s-check', `<center><b>Bienvenido</b> ${params.user}.</center>`, 'success', 500, 2000);
                $('#formLogin').trigger("reset");
                $('#btnLogin').attr("disabled", true);
                setTimeout(() => { $(location).attr('href','/'); }, 2500);
            }
        }
    }).catch(e => console.error(e));
});
/*
$('#').click((e)=>{
    e.preventDefault();
    const file = $('#customFile').val();
    console.log({file});
    let url = "client/load";
    fetch(url, {
        method: 'POST',
        body: file,
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }).then((response) => {
        if (response.status === 200)
            return response.text();
    }).then((data) => {
        if (data) {
            console.log({data});
        }
    });
});*/