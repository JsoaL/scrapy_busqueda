const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ConceptSchema = new Schema({
    concepto: String,
    definicion: String,
    imagen: String
});

module.exports = mongoose.model('Concept', ConceptSchema);