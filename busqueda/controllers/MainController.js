const Concept = require("../models/Concept");
const Product = require("../models/Product");
const utils = require("../utils/Utileria");
const fs = require('fs');
const MainController = {};

const formatResponse = (items)=>{
    let arrayResultado = [];
    let array = [];
    for (let i = 1; i < items.length + 1; i++) {
        if ((i % 3) === 0) {
            array.push(items[i - 1]); arrayResultado.push(array);
            array = [];
        } else {
            if (i === items.length - 1) {
                array.push(items[i - 1]);
                array.push(items[i]);
                arrayResultado.push(array);
                break;
            }
            else if (i === items.length) {
                array.push(items[i - 1])
                arrayResultado.push(array);
                break;
            } else  array.push(items[i - 1]);
        }
    }
    return arrayResultado;
};

const indexConcepts = (res, campo, resultProduct, isWebService) =>{
    utils.changeFile("@@@", campo, '/home/busqueda/arana/prueba/prueba/spiders/dictionary.py');
    let comando = "rm /home/busqueda/public/results/concepto.json && ";
    comando += "cd arana/prueba/prueba/spiders/ && ";
    comando += "scrapy crawl dictionary -o /home/busqueda/public/results/concepto.json";
    utils.execute(comando, () => {
        const file = (fs.readFileSync('/home/busqueda/public/results/concepto.json', 'utf8'));
        if(file){
            const result = JSON.parse(file);
            if(result.length !== 0){
                for (const item of result) {
                    var concepto = new Concept(item);
                    concepto.save((err) => {
                        if (err) { console.log('Error: ', err); return; }
                        console.log("Successfully created a concept.");
                    });
                }
                utils.changeFile(campo, "@@@", '/home/busqueda/arana/prueba/prueba/spiders/dictionary.py');
                if(isWebService) res.send(result);
                else if(resultProduct.length !== 0)res.render('clients/index', { items: formatResponse([...result, ...resultProduct]) });
                else indexProducts(res, campo, result, false);
            }
        } else {
            utils.changeFile(campo, "@@@", '/home/busqueda/arana/prueba/prueba/spiders/dictionary.py');
            if(isWebService) res.send([]);
            else if(resultProduct.length !== 0) res.render('clients/index', { items: formatResponse([...resultProduct]) });
            else indexProducts(res, campo, [], false);
        };
    }, () => { res.sendStatus(500); });
};

const indexProducts = (res, campo, resultConcept, isWebService) =>{
    utils.changeFile("@@@",campo,'/home/busqueda/arana/prueba/prueba/spiders/ejemplo.py');
    let comando = "rm /home/busqueda/public/results/file.json && ";
    comando += "cd arana/prueba/prueba/spiders/ && ";
    comando += "scrapy crawl ejemplo -o /home/busqueda/public/results/file.json";
    utils.execute(comando,()=> {
        const file = (fs.readFileSync('/home/busqueda/public/results/file.json', 'utf8'));
        if(file){
            const result = JSON.parse(file);
            if(result.length !== 0){
                for (const item of result) {
                    var producto = new Product(item);
                    producto.save((err) => {
                        if (err) { console.log('Error: ', err); return; }
                        console.log("Successfully created a product.");
                    });
                }
                utils.changeFile(campo, "@@@", '/home/busqueda/arana/prueba/prueba/spiders/ejemplo.py');
                if(isWebService) res.send(result);
                else if(typeof resultConcept !== "undefined") res.render('clients/index',{items: formatResponse([...resultConcept,...result])});
                else res.sendStatus(200);
            }
        } else {
            utils.changeFile(campo, "@@@", '/home/busqueda/arana/prueba/prueba/spiders/dictionary.py');
            if(isWebService) res.send([]);
            else if(typeof resultConcept !== "undefined") res.render('clients/index', { items: formatResponse([...resultConcept]) });
            else res.sendStatus(500);
        }
    },()=>{ res.sendStatus(500); });
};

const fetchDictionary = (params)=>{
    params.isFetchedBy = true;
    utils.asyncGetReq(params,`http://localhost:3000/servicioDiccionario`).then(result => {
        if (result) {
            return result;
        }
    }).catch(e => console.error(e));
};

const fetchAmazon = (params)=>{
    params.isFetchedBy = true;
    utils.asyncGetReq(params,`http://localhost:3000/servicioCompras`).then(result => {
        if (result) {
        }
    }).catch(e => console.error(e));
};

MainController.test = (req, res, next) => {
    const parametros = req.query; //{campo:"asdasd"}
    /*utils.asyncGetReq(parametros,`http://localhost:3000/servicioCompras`).then(data => {
        if (data) {
            res.send(data);
        }
    }).catch(e => console.error(e));*/
    res.send({test:"isThis_A_Test?"});
};

MainController.doSearch = (req, res, next) => {
    const parametros = req.body;
    const arrayWords = parametros['campo'].split(" ");
    let expresion = "";
    for (const word of arrayWords) expresion += `(?=.*${word})`;
    Concept.find({concepto: new RegExp(`${parametros['campo'].trim()}`,'i')}).exec((err, resultConcept) => {
        if (err) { console.error(err); return; }
        Product.find({producto: new RegExp(`^${expresion}.*$`,'i')}).exec((err, resultProduct) => {
            if (err) { console.error(err); return; }
            const field = parametros.campo;
            const boolConcept = resultConcept.length === 0;
            const boolProduct = resultProduct.length === 0;
            console.log(resultConcept.length);
            console.log(resultProduct.length);
            if(boolConcept && boolProduct) indexConcepts(res, field, resultProduct, false);
            else if (boolConcept) indexConcepts(res, field, resultProduct, false);
            else if (boolProduct) indexProducts(res, field, resultConcept, false);
            else res.render('clients/index',{items: formatResponse([...resultConcept,...resultProduct])});
        });
    });
};

MainController.servicioDiccionario = (req, res, next) =>{
    const parametros = req.query;
    const arrayWords = parametros['campo'].split(" ");
    Concept.find({concepto: new RegExp(`${parametros['campo'].trim()}`,'i')},'-_id -__v').exec((err, resultConcept) => {
        if (err) { console.error(err); return; }
        const field = parametros.campo;
        const boolConcept = resultConcept.length === 0;
        console.log(resultConcept.length);
        if (boolConcept) indexConcepts(res, field, [], true);
        else res.send( resultConcept );
    });
};

MainController.servicioCompras = (req, res, next) =>{
    const parametros = req.query;
    const arrayWords = parametros['campo'].split(" ");
    let expresion = "";
    for (const word of arrayWords) expresion += `(?=.*${word})`;
    Product.find({producto: new RegExp(`^${expresion}.*$`,'i')},'-_id -__v').exec((err, resultProduct) => {
        if (err) { console.error(err); return; }
        const field = parametros.campo;
        const boolProduct = resultProduct.length === 0;
        console.log(resultProduct.length);
        if(boolProduct) indexProducts(res, field, [], true);
        else res.send(resultProduct);
    });
};

module.exports = MainController;