const Concept = require("../models/Concept");
const ConceptController = {};
const utils = require("../utils/Utileria");

ConceptController.create = function (req, res){
    let comando = "rm /home/busqueda/public/results/file.json &&";
    comando += "cd arana/prueba/prueba/spiders/ &&";
    comando += "scrapy crawl ejemplo -o /home/busqueda/public/results/file.json";
    utils.execute(comando,()=>{
        res.redirect("/results/file.json");
    },()=>{
        res.sendStatus(500);
    });
};

ConceptController.save = function (req, res){
    var concepto = new Concept(req.body);
    concepto.save(function (err) {
        if (err) { console.log('Error: ', err); return; }
        console.log("Successfully created a user.");
        res.redirect("/admin/create");
    });
};

module.exports = ConceptController;

